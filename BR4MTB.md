# ТОРГОВАЯ СИСТЕМА ДЛЯ МОСКОВСКОЙ БИРЖИ
## ТРЕБОВАНИЯ

### 1. Цели и объекты бизнеса
**Цели:**
1. Разработать и внедрить торгового бота для операций на Московской бирже, который сможет автоматически отслеживать и анализировать торговые данные в реальном времени.
2. Торговый бот должен максимизировать прибыль пользователя, минимизируя при этом возможные риски, используя современные техники и алгоритмы анализа данных и машинного обучения для определения оптимальных моментов для покупки и продажи ценных бумаг.
3. Торговый бот должен обеспечить высокий уровень безопасности данных и торговых операций пользователя.
4. Пользовательский интерфейс торгового бота должен быть интуитивно понятным и удобным:
   - для управления функциями и настройками бота;
   - для наблюдения за торговыми операциями, их эффективностью;
   - для наблюдения за работой системы в целом.

**Объекты бизнеса:**
1. **Пользователь:** Трейдер, который использует торгового бота для автоматизации своих торговых операций на Московской бирже. Пользователь устанавливает параметры и стратегии для бота (настройки), следит за его работой и корректирует настройки при необходимости.
2. **Торговая система:**
    
    1.1.**Торговый бот:** Программное обеспечение, которое автоматизирует торговые операции пользователя на Московской бирже. Бот должен быть способен анализировать торговые данные и принимать решения о покупке или продаже на основе установленных пользователем настроек и современных алгоритмов искусственного интеллекта.

    1.2. **Google Sheets:** Таблицы для установки параметров, стратегии (настроек), сохранения лога работы и лога операций бота.

    1.3. **Telegram bot:** Телеграм бот для запуска, остановки бота, для подтверждения реальных финансовых операций покупки или продажи, для оповещения о критических ошибках или состояниях.
3. **Московская биржа:** Финансовая площадка, на которой торговый бот будет проводить операции. Биржа предоставляет данные о ценах, объемах и других торговых показателях, которые бот будет использовать для анализа и принятия решений.
4. **Брокер:** Лицензированная компания, которая обеспечивает доступ пользователя к проведению операций на бирже, осуществляет функцию посредника между Пользователем и Московской биржей, предоставляя техническую инфраструктуру и услуги для проведения торговых операций.

```plantuml
@startuml
    left to right direction
   
    title MOEX Trading Bot System - HIGH LEVEL USE CASE DIAGRAM
   
    actor "MOEX" as exchange
    actor "Broker" as broker
    actor "User" as user #Orange
   
    rectangle "Telegram bot (user interface)" as telegram #LightBlue
    rectangle "Google Sheets (data base)" as sheets #LightYellow
    rectangle "Trading bot" as TradingBot #LightGreen
   
    user --> telegram : Controls Bot via Commands
    user --> sheets : Updates Parameters and Monitors Bot
   
    sheets --> TradingBot : Provides Parameters
    TradingBot --> sheets : Logs Transactions and Bot Activity
   
    telegram --> TradingBot : Sends User Commands
    TradingBot --> telegram : Sends Trade Confirmations and Results
   
    exchange --> TradingBot : Provides Trading Data
    TradingBot --> broker : Performs Trading Operations
@enduml
```

```plantuml

@startuml
    left to right direction

    title Trading Bot System - C4 LEVEL 3: COMPONENT DIAGRAM

   actor User #Orange

    package "User Interface" {
      [Telegram Bot] #lightBlue
    }

    package "Trading Bot" #LightGreen {
      [Trading Bot Controller]
      [Data Collection Module]
      [Data Cleaning Module]
      [Feature Extraction Module]
      [Risk Assessment Module]
      [Market Prediction Module]
      [Trading Strategy Module]
      [Order Execution Module]
      [Effectiveness Measurement Module]
      [Strategy and Model Adaptation Module]
    }

    database "Google Sheets" #LightYellow {
      [Trading Bot Settings]
      [Trading Bot Log]
      [Trade History]
    }

    cloud "External Systems" {
      [Broker]
      [MOEX]
    }


    [User] -right-> [Telegram Bot]
    [Telegram Bot] -down-> [Trading Bot Controller]
    [Trading Bot Controller] --> [Trading Bot Settings] : Use
    [Trading Bot Controller] --> [Trading Bot Log] : Use
    [Trading Bot Controller] --> [Trade History] : Use
    [Trading Bot Controller] --> [Data Collection Module] : Use
    [Trading Bot Controller] --> [Data Cleaning Module] : Use
    [Trading Bot Controller] --> [Feature Extraction Module] : Use
    [Trading Bot Controller] --> [Risk Assessment Module] : Use
    [Trading Bot Controller] --> [Market Prediction Module] : Use
    [Trading Bot Controller] --> [Trading Strategy Module] : Use
    [Trading Bot Controller] --> [Order Execution Module] : Use
    [Trading Bot Controller] --> [Effectiveness Measurement Module] : Use
    [Trading Bot Controller] --> [Strategy and Model Adaptation Module] : Use
    [Data Collection Module] -right-> [MOEX] : Use
    [Order Execution Module] -right-> [Broker] : Use
    [Broker] --> [MOEX] : Use

@enduml

```
### 2. Описание бизнес-процессов
#### 2.1 Настройка Торгового Бота
Процесс начинается с пользователя, который настраивает параметры для торгового бота в Google Sheets. Здесь он может установить параметры, такие как `is_trading_mode`, указывающий, будет ли бот торговать в реальном времени (True) или в симуляционном режиме (False). Также пользователь может указать список тикеров для торговли и выбрать торговую стратегию.

#### 2.2 Мониторинг Торгового Бота
Пользователь может отслеживать активность торгового бота в реальном времени через Google Sheets. Вся активность, такая как запросы данных, чистка данных, прогнозы рынка и транзакции, логируется для прозрачности и возможности последующего анализа.

#### 2.3 Проверка транзакций и эффективности Торгового Бота
Пользователь может проверять историю транзакций и эффективность торгового бота через Google Sheets. Здесь он может увидеть историю совершенных сделок, а также оценить эффективность торгового бота на основе результата этих сделок.

#### 2.4 Запуск Торгового Бота
Для запуска торгового бота пользователь отправляет команду старт через Telegram бота. Бот, в свою очередь, передает эту команду Контроллеру торгового бота. Контроллер запрашивает все настройки от Google Sheets и логирует результаты. Контроллер возвращает результаты и настройки в Telegram бота, который затем показывает их пользователю.

#### 2.5 Торговый процесс
После запуска торгового бота начинается бесконечный цикл сбора данных, чистки данных, извлечения признаков, оценки риска, прогнозирования рынка, определения торговой стратегии и выполнения ордеров. Все эти процессы происходят внутри бота и включают различные модули, которые вместе выполняют всю работу торгового бота.

#### 2.6 Покупка/Продажа
Сигнал покупк/продажи обрабатывает модуль исполнения ордеров (OEM). Если бот находится в реальном режиме (`is_trading_mode = True`), он отправляет запрос на подтверждение операции пользователю через Telegram бота. После подтверждения пользователя OEM осуществляет операцию через брокера и Московскую биржу. Если бот находится в симуляционном режиме (`is_trading_mode = False`), он просто симулирует результат операции.

#### 2.7 Измерение эффективности
Эффективность торгового бота измеряется на основе истории транзакций. Если эффективность упадет ниже определенного порога, торговый бот будет остановлен. В противном случае, если бот находится в симуляционном режиме, модуль адаптации стратегии и модели обновит модель и стратегию на основе эффективности.

#### 2.8 Остановка Торгового Бота
Пользователь может остановить торгового бота в любой момент, отправив команду стоп через Telegram бота. Бот передает команду контроллеру, который затем останавливает работу бота и логирует это действие.

```plantuml
@startuml
    title MOEX Trading Bot System - LOW-LEVEL SEQUENCE DIAGRAM

	actor "User" as user #Orange
	participant "Telegram Bot \n(user interface)" as telegram #LightBlue

	box "Google Sheets" #LightYellow
		participant "Trading\nBot\nSettings" as settings
		participant "Trading\nBot\nLog" as log
		participant "Trade\nHistory" as history
	end box

	box "Trading bot" #LightGreen
		participant "Trading\nBot\nController" as Controller
		participant "Data\nCollection\nModule" as DCOM
		participant "Data\nCleaning\nModule" as DCLM
		participant "Feature\nExtraction\nModule" as FEM
		participant "Risk\nAssessment\nModule" as RAM
		participant "Market\nPrediction\nModule" as MPM
		participant "Trading\nStrategy\nModule" as TSM
		participant "Order\nExecution\nModule" as OEM
        participant "Effectiveness\nMeasurement\nModule" as EMM
        participant "Strategy and Model\nAdaptation\nModule" as SMAM
	end box

	actor "Broker" as broker
	actor "MOEX" as exchange

    opt The User decides to sets options
        user -> settings : Sets\nis_trading_mode = True|False,\ntickers = [list of tickers],\nstrategy = 'some_strategy'
        user -> settings : Sets other settings
    end

    opt The User decides to monitors Trading Bot
        user -> log : Monitors Trading Bot
    end

    opt The User decides to checks transactions and effectiveness of the Trading Bot
        user -> history : Checks transactions
        user -> history : Checks effectiveness
    end

    opt The User decides to start the Trading Bot
        user -> telegram : Sends start command
        telegram -> Controller : Passes start command
        Controller -> settings : Requests all settings
        settings --> Controller : Returns all settings
        Controller --> telegram : Sends result of start and all settings
        telegram --> user : Shows start and all settings
        Controller -> log : Logs start and all settings

        loop forever
            Controller -> Controller: Calcs of the trading data period\ndepending on is_trading_mode
            Controller -> DCOM: Sends period of trading data
            DCOM -> exchange : Requests trading data online according to period
            DCOM -> log : Logs request
            exchange --> DCOM : Returns trading data
            DCOM -> log : Logs result of request

            DCOM -> DCLM : Sends raw data
            DCLM -> DCLM : Cleans and formats data
            DCLM -> log : Logs result of operation
            DCLM -> FEM : Sends cleaned data
            DCLM -> log : Logs result of operation
            FEM -> FEM : Extracts features
            FEM -> log : Logs result of operation
            FEM -> RAM : Sends features
            RAM -> RAM : Assesses risk
            RAM -> log : Logs result of operation
            RAM -> MPM : Sends risk ratings
            FEM -> MPM : Sends features
            MPM -> MPM : Predicts market
            MPM -> log : Logs result of operation
            MPM -> TSM : Sends market predictions
            RAM -> TSM : Sends risk ratings
            TSM -> TSM : Decides trading strategy
            TSM -> log : Logs result of operation
            TSM -> OEM : Sends trade signals

            opt Buy/Sell Signal Met
                alt is_trading_mode = True
                    OEM -> telegram : Sends confirmation request for buy/sell
                    telegram -> user : Shows confirmation request for buy/sell
                    user -> telegram : Confirms/Denies of transaction
                    telegram -> OEM : Passes result of user confirmation
                    OEM -> log : Logs result of user confirmation

                    alt User confirmed transaction
                        OEM -> broker : Performs buy/sell operation
                        OEM -> log : Logs transaction
                        broker -> exchange : Performs buy/sell operation
                        exchange --> broker : Confirms buy/sell operation
                        broker --> OEM : Confirms buy/sell operation
                        OEM -> log : Logs result of transaction
                        OEM -> history : Logs result of transaction in trade history
                    end
                    OEM --> telegram : Sends result of transaction
                    telegram --> user : Shows result of transaction
                else is_trading_mode = False
                    OEM -> OEM : Simulates the performance of buy/sell operations
                    OEM -> log : Logs result of simulation
                    OEM -> history : Logs result of simulation in trade history
                end

            end

            EMM -> history : Requests transaction history from the last update of the Strategy and Model to the most recent transaction
            history --> EMM : Returns Trade History

            EMM -> EMM: Measurements effectiveness
            EMM -> log : Logs effectiveness
            EMM -> history : Logs effectiveness in trade history
            EMM --> telegram : Sends effectiveness
            telegram --> user : Shows effectiveness
            alt is_trading_mode = True
                break Effectiveness below settings.threshold
                        EMM -> log : Logs stop command
                        EMM --> telegram : Sends result of stop command
                        telegram --> user : Shows result of stop command
                end
            else  is_trading_mode = False
                EMM -> SMAM : Sends effectiveness
                SMAM -> SMAM : Adapts model and strategy
                SMAM -> MPM : Updates model
                SMAM -> log : Logs model update
                SMAM -> TSM : Updates strategy
                SMAM -> log : Logs strategy update
            end
            break The User decides to stop the Trading Bot
                user -> telegram : Sends stop command
                telegram -> Controller : Passes stop command
                Controller -> log : Logs stop command
                Controller --> telegram : Sends result of stop command
                telegram --> user : Shows stop command
            end
        end
    end
@enduml
```

### 3. Сценарии использования и пользовательские истории
1. **Настройка**
   - **Сценарий использования**: 
     - Пользователь может настроить торгового бота, сохраняя необходимые параметры через Google Sheets. Это может включать выбор торгового режима, списка тикеров, стратегии и прочих параметров.
   - **Пользовательская история**:
     - В качестве пользователя, я хочу иметь возможность настроить параметры торгового бота, чтобы бот выполнял торговые операции согласно настройкам.
2. **Мониторинг**
   - **Сценарий использования**: 
     - Пользователь может отслеживать действия и результаты, просматривая логи бота в Google Sheets.
   - **Пользовательская история**:
     - В качестве пользователя, я хочу отслеживать действия и результаты торгов бота в реальном времени, чтобы быть в курсе его работы и эффективности.
     - В качестве пользователя, я хочу иметь возможность получать графики и диаграммы на основе лога бота, чтобы упростить анализ его работы.
     - В качестве пользователя, я хочу получать уведомления о критических событиях в работе торгового бота, чтобы своевременно реагировать на изменения.
     - В качестве пользователя, я хочу иметь возможность настроить уровень детализации сообщений от бота, чтобы получать только ту информацию, которая мне действительно нужна.
3. **Проверка Транзакций и Эффективности**
   - **Сценарий использования**: 
     - Пользователь может проверять историю транзакций и эффективность бота в Google Sheets.
   - **Пользовательская история**:
     - В качестве пользователя, я хочу проверять историю транзакций и эффективность торгового бота, чтобы понимать, насколько успешно бот выполняет свои торговые операции.
     - В качестве пользователя, я хочу иметь возможность получать графики и диаграммы на основе данных о транзакциях и эффективности торгового бота, чтобы упростить анализ его работы.
     - В качестве пользователя, я хочу, чтобы бот автоматически останал торговые транзакции при снижении эффективности ниже определенного уровня.
     - В качестве пользователя, я хочу сравнить производительность бота с эталонными индексами или аналогичными финансовыми инструментами, чтобы более объективно оценить его эффективность.
4. **Адаптация модели предстказания и стратегии**
   - **Сценарий использования**: 
     - При начале работы с ботом или снижении эффективности торговых операций пользователь адаптирует модели для предстказания и стратегии принятия решений бота.
   - **Пользовательская история**:
     - В качестве пользователя, я хочу иметь возможность изменять стратегию торговли, улучшать модель предсказания с целью увеличить эффективность выполнения торговых операций.
     - В качестве пользователя, я хочу протестировать изменения в торговой стратегии или модели прогнозирования, прежде чем применять их, чтобы убедиться, что они не окажут негативного влияния на производительность бота.
     - В качестве пользователя, я хочу, чтобы система автоматически предлагала оптимизацию текущей стратегии и модели на основе последних данных о торгах, чтобы повысить эффективность торгового бота.
5. **Управление**
   - **Сценарий использования**: 
     - Пользователь может запустить или остановить бота, отправив команду старта или завершения через Telegram.
   - **Пользовательская история**:
     - В качестве пользователя, я хочу иметь возможность запустить или остановить бота.
     - В качестве пользователя, я хочу получать подтверждение от бота о выполнении команды запуска или остановки, чтобы быть уверенным в его текущем статусе.
     - В качестве пользователя, я хочу, чтобы бот автоматически определял период, когда открыта биржа, и осуществлял торговлю в этот период.
6. **Подтверждение операций**
   - **Сценарий использования**: 
     - Пользователь должен подтверждать все финансовые операции бота через Telegram.
   - **Пользовательская история**:
     - В качестве пользователя, я хочу иметь возможность контролировать реальные покупки и продажи бота.
     - В качестве пользователя, я хочу иметь возможность настраивать автоматическое подтверждение операций в зависимости от уровня риска, чтобы экономить свое время и не подтверждать рутинные операции.
```plantuml

@startuml
    title Trading Bot System - MID LEVEL USE CASE DIAGRAM

    skinparam packageStyle rectangle

    actor "User" as user #Orange

    rectangle "Telegram Bot" as telegram #LightBlue
    actor "Broker" as broker
    actor "MOEX" as exchange

    user -- telegram : Interacts

    rectangle "Google Sheets" #LightYellow {
        usecase "Set Options" as UC1
        usecase "Monitor Trading Bot" as UC2
        usecase "Check Transactions & Effectiveness" as UC3
    }

    rectangle "Trading Bot" #LightGreen {
        usecase "Start Trading Bot" as UC4
        usecase "Trading Data Calculation" as UC5
        usecase "Request Trading Data" as UC6
        usecase "Clean & Format Data" as UC7
        usecase "Feature Extraction" as UC8
        usecase "Risk Assessment" as UC9
        usecase "Market Prediction" as UC10
        usecase "Decide Trading Strategy" as UC11
        usecase "Execute Trade" as UC12
        usecase "Measure Effectiveness" as UC13
        usecase "Adapt Model & Strategy" as UC14
        usecase "Stop Trading Bot" as UC15
    }

    user -- UC1 : Sets options
    user -- UC2 : Monitors Bot
    user -- UC3 : Checks transactions & effectiveness
    user -- UC3 : Checks transactions & effectiveness

    UC1 -- UC4 : Triggers start
    UC4 ..> UC5 : Includes
    UC5 ..> UC6 : Includes
    UC6 -- exchange : Interacts
    UC6 ..> UC7 : Includes
    UC7 ..> UC8 : Includes
    UC8 ..> UC9 : Includes
    UC8 ..> UC10 : Includes
    UC9 ..> UC10 : Includes
    UC9 ..> UC11 : Includes
    UC10 ..> UC11 : Includes
    UC11 ..> UC12 : Includes
    UC12 -- broker : Interacts
    UC12 -- UC13 : Triggers measurement
    UC13 ..> UC14 : Includes
    UC14 ..> UC10 : Updates model
    UC14 ..> UC11 : Updates strategy
    UC1 -- UC15 : Triggers stop

    telegram -- UC4 : Interacts
    telegram -- UC12 : Interacts
    telegram -- UC13 : Interacts
    telegram -- UC15 : Interacts
@enduml

```
### 4. Функциональные требования
Все модули должны вести лог своей работы, и обеспечивать сохранение и восстановление своего состояния.

1. **Модуль управления (Trading Bot Controller)**: должен иметь функциональность запуска и остановки бота, а также возможность получения и обработки настроек бота.
2. **Модуль сбора данных (Data Collection Module)**: этот модуль отвечает за сбор торговых данных в реальном времени, частота сбора данных зависит от выбранной стратегии. Он должен иметь функциональность для запроса данных от биржи и передачи их в модуль очистки данных.
3. **Модуль очистки данных (Data Cleaning Module)**: основная функция этого модуля – очистка и форматирование сырых данных, полученных от модуля сбора данных. Очищенные данные затем передаются в модуль извлечения признаков.
4. **Модуль извлечения признаков (Feature Extraction Module)**: этот модуль анализирует очищенные данные и извлекает из них важные признаки, которые затем передаются в модули оценки риска и прогнозирования рынка.
5. **Модуль оценки риска (Risk Assessment Module)**: основная функция этого модуля - оценивать риски на основе признаков, полученных от модуля извлечения признаков. Оценки риска затем передаются в модули прогнозирования рынка и определения стратегии торговли.
6. **Модуль прогнозирования рынка (Market Prediction Module)**: этот модуль использует признаки и оценки риска для создания прогнозов рыночных условий. Эти прогнозы затем передаются в модуль определения стратегии торговли.
7. **Модуль стратегии торговли (Trading Strategy Module)**: на основе прогнозов рынка и оценок риска этот модуль определяет оптимальную торговую стратегию и генерирует сигналы для покупки или продажи.
8. **Модуль исполнения ордеров (Order Execution Module)**: этот модуль отвечает за выполнение торговых операций на основе сигналов от модуля стратегии торговли. Модуль запрашивает у пользователя подтверждение для выполнения финансовых транзакций. Он также должен иметь функционал для симуляции операций, когда бот находится в режиме тестирования.
9. **Модуль измерения эффективности (Effectiveness Measurement Module)**: основная функция этого модуля – измерение эффективности торгового бота, основываясь на истории торгов.
10. **Модуль адаптации стратегии и модели (Strategy and Model Adaptation Module)**: этот модуль отвечает за обновление модели и стратегии в ответ на изменение эффективности бота.
11. **Telegram Bot (пользовательский интерфейс)**: обеспечивает возможность пользователям взаимодействовать с торговым ботом через привычный интерфейс мессенджера Telegram.
12. **Google Sheets (настройки бота, журнал и история торгов)**: эти компоненты обеспечивают хранение настроек бота, логов его работы и истории сделок, а также предоставляют пользователю доступ к этим данным.

```plantuml
@startuml
    title Trading Bot System - CLASS DIAGRAM

    class User {
        +setTradingBotSettings()
        +monitorTradingBot()
        +checkTransactions()
        +checkEffectiveness()
        +startTradingBot()
        +stopTradingBot()
        +confirmTransaction()
    }

    class TelegramBot {
        +receiveCommand()
        +passCommand()
        +requestConfirmation()
        +sendEffectiveness()
        +showResult()
    }

    class TradingBotSettings {
        -is_trading_mode : Boolean
        -tickers : String[]
        -strategy : String
        +setSettings()
        +getSettings()
    }

    class TradingBotLog {
        +writeLog()
    }

    class TradeHistory {
        +getTransactions()
        +getEffectiveness()
        +logTransaction()
        +logEffectiveness()
    }

    class TradingBotController {
        +start()
        +stop()
        +calcTradingPeriod()
        +handleRequest()
        +sendResult()
    }

    class DataCollectionModule {
        +getTradingData()
        +sendRawData()
    }

    class DataCleaningModule {
        +receiveRawData()
        +cleanAndFormatData()
        +sendCleanedData()
    }

    class FeatureExtractionModule {
        +receiveCleanedData()
        +extractFeatures()
        +sendFeatures()
    }

    class RiskAssessmentModule {
        +receiveFeatures()
        +assessRisk()
        +sendRiskRatings()
    }

    class MarketPredictionModule {
        +receiveRiskRatings()
        +receiveFeatures()
        +predictMarket()
        +sendMarketPredictions()
    }

    class TradingStrategyModule {
        +receiveMarketPredictions()
        +receiveRiskRatings()
        +decideStrategy()
        +sendTradeSignals()
    }

    class OrderExecutionModule {
        +receiveTradeSignals()
        +requestConfirmation()
        +performOperation()
        +logTransaction()
        +sendResult()
    }

    class EffectivenessMeasurementModule {
        +requestTransactionHistory()
        +measureEffectiveness()
        +logEffectiveness()
        +sendEffectiveness()
    }

    class StrategyAndModelAdaptationModule {
        +receiveEffectiveness()
        +adaptModelAndStrategy()
        +updateModel()
        +updateStrategy()
    }

    class Broker {
        +performOperation()
    }

    class MOEX {
        +provideTradingData()
        +confirmOperation()
    }

    User -- TradingBotSettings : sets settings
    User -- TradingBotLog : monitors log
    User -- TradeHistory : checks
    User -- TelegramBot : commands

    TelegramBot -- TradingBotController : passes commands

    TradingBotController -- TradingBotSettings : uses
    TradingBotController -- TradingBotLog : writes

    TradingBotController -- DataCollectionModule : controls
    DataCollectionModule -- MOEX : requests
    DataCollectionModule -- DataCleaningModule : sends raw data

    DataCleaningModule -- FeatureExtractionModule : sends cleaned data
    FeatureExtractionModule -- RiskAssessmentModule : sends features
    FeatureExtractionModule -- MarketPredictionModule : sends features
    RiskAssessmentModule -- MarketPredictionModule : sends risk ratings
    RiskAssessmentModule -- TradingStrategyModule : sends risk ratings

    MarketPredictionModule -- TradingStrategyModule : sends market predictions
    TradingStrategyModule -- OrderExecutionModule : sends trade signals

    OrderExecutionModule -- TelegramBot : requests confirmation
    OrderExecutionModule -- Broker : performs operations
    OrderExecutionModule -- TradeHistory : logs transactions

    Broker -- MOEX : performs operations

    EffectivenessMeasurementModule -- TradeHistory : requests transaction history
    EffectivenessMeasurementModule -- TelegramBot : sends effectiveness
    EffectivenessMeasurementModule -- StrategyAndModelAdaptationModule : sends effectiveness

    StrategyAndModelAdaptationModule -- MarketPredictionModule : updates model
    StrategyAndModelAdaptationModule -- TradingStrategyModule : updates strategy
@enduml
```

### 5. Нефункциональные требования
#### 5.1. Производительность
1. **Эффективность обработки данных:** Система должна быть способна обрабатывать большие объемы торговых данных в режиме реального времени, чтобы обеспечить быстрые и своевременные решения по торговле.
2. **Отклик системы:** Время ответа на запросы пользователей и брокера должно быть минимальным, желательно в пределах нескольких секунд.
3. **Скорость обучения модели:** Система должна быть способна обновлять и адаптировать модели и стратегии в период отсуствия торгов: ночное время, выходные или праздничные дни.
#### 5.2. Надежность и доступность
1. **Отказоустойчивость:** Система должна быть способна восстановиться после сбоев и продолжить работу с места, где была прервана.
2. **Резервное копирование и восстановление:** Система должна поддерживать возможность резервного копирования и восстановления информации о торговле и настройках пользователя.
3. **Доступность:** Система должна быть доступна для использования 24/7, за исключением периодов планового обслуживания.
#### 5.3. Безопасность
1. **Конфиденциальность:** Система должна обеспечивать конфиденциальность данных пользователя, включая информацию о торговле и настройках пользователя.
2. **Интеграция с брокером:** Все операции, связанные с выполнением операций покупки/продажи, должны проводиться в защищенном режиме.
3. **Управление доступом:** Доступ к системе должен быть разграничен на основе прав пользователей.
#### 5.4. Масштабируемость
1. **Вертикальная и горизонтальная масштабируемость:** Система должна быть способна расширяться для поддержки увеличения количества данных и сложности обработки.
#### 5.5. Удобство использования
1. **Интерфейс пользователя:** Интерфейс должен быть интуитивно понятным и простым в использовании. Интерфейс должен предоставлять функции для контроля и мониторинга работы торгового бота.
2. **Отчетность:** Система должна предоставлять подробные отчеты о производительности бота, истории торгов и эффективности.
#### 5.6. Совместимость
1. **Интеграция с MOEX и брокерами:** Система должна быть совместима с API MOEX и поддерживаемых брокеров для обмена информацией о торговле.
2. **Интеграция с Google Sheets:** Система должна быть способна интегрироваться с Google Sheets для обработки настроек торгового бота и журнала.

```plantuml

@startuml
    left to right direction

    title Trading Bot System - DEPLOYMENT DIAGRAM

    cloud Internet {
    }
    
    actor User #Orange

    package "User Device" {
        [Telegram App] as TelegramApp #LightBlue
    }

    package "Application Server" {
        node "Trading Bot" as TradingBot #LightGreen {
            [Trading Bot Controller]
            [Data Collection Module]
            [Data Cleaning Module]
            [Feature Extraction Module]
            [Risk Assessment Module]
            [Market Prediction Module]
            [Trading Strategy Module]
            [Order Execution Module]
            [Effectiveness Measurement Module]
            [Strategy and Model Adaptation Module]
        }
    }

    node MOEXServer {
        [MOEX]
    }

    node BrokerageServer {
        [Broker]
    }

    database "Google Sheets" as GoogleSheets #LightYellow {
        [Trading Bot Settings]
        [Trading Bot Log]
        [Trade History]
    }

    User --> TelegramApp
    TelegramApp --> Internet
    Internet <-- TradingBot 
    Internet <-- BrokerageServer
    Internet <-- MOEXServer
    Internet <-- GoogleSheets

@enduml

```

### 6. Ограничения и предположения
**Предположения:**
1. **Доступность и своевременность данных:** Система основана на предположении, что данные с Московской биржи (MOEX) будут доступны в режиме реального времени. Задержки или прерывания в передаче данных могут привести к сбоям в работе бота.
2. **Стабильность API брокера:** Предполагается, что API брокера всегда будет работать надежно. Отказы или проблемы с API могут привести к задержкам или ошибкам в исполнении торговых операций.
3. **Безошибочность модулей:** Предполагается, что каждый модуль торгового бота работает без ошибок и точно выполняет свои функции. Это относится к модулям сбора данных, оценки рисков, прогнозирования рынка, выбора стратегии, исполнения заявок и оценки эффективности.
4. **Адаптивность моделей:** Предполагается, что модель прогнозирования и стратегия торговли могут быть адаптированы в соответствии с эффективностью бота. В реальных условиях эта адаптация может потребовать значительного времени и ресурсов.

**Ограничения:**
1. **Зависимость от настроек:** Работа бота зависит от правильности настроек, введенных пользователем. Неправильные настройки могут привести к потере прибыли или другим нежелательным результатам.
2. **Ограниченность предсказательных возможностей:** Хотя модуль прогнозирования рынка использует передовые методы машинного обучения, предсказание поведения рынка все еще представляет собой сложную и неопределенную задачу.
3. **Зависимость от торгового режима:** Поведение бота и его эффективность могут значительно изменяться в зависимости от выбранного торгового режима (реальный или симуляционный).
4. **Пользовательский ввод:** Система предполагает, что пользователь будет подтверждать или отклонять торговые сигналы в режиме реального времени. Если пользователь не в состоянии это сделать, система может не совершать потенциально прибыльные сделки.
5. **Прерывания работы:** Без контроля со стороны пользователя, торговый бот может остановиться, если эффективность торговли упадет ниже установленного порога. Это может привести к прерыванию торговли в неподходящий момент.
6. **Ограниченное время работы:** Бот работает только во время торговых сессий Московской биржи. Время работы биржи может изменяться, что может повлиять на эффективность бота. 
7. **Зависимость от стабильности интернет-соединения:** Поскольку бот взаимодействует с Московской биржей и брокером через Интернет, любые сбои или прерывания в подключении могут повлиять на его работу.

### 7. Критерии приемки
1. **Настройки бота (Trading Bot Settings):** Бот должен корректно сохранять и использовать настройки, заданные пользователем. Это включает в себя режим торговли (`is_trading_mode`), выбранные тикеры и выбранную торговую стратегию. Пользовательские изменения в настройках должны быть немедленно применены ботом.
2. **Мониторинг работы бота (Trading Bot Log):** Все операции бота должны быть правильно зарегистрированы в логах. Это должно включать информацию о старте и остановке бота, а также о всех действиях, выполняемых в рамках его работы. 
3. **История торговых операций и эффективность бота (Trade History):** Бот должен точно отслеживать все совершенные торговые операции, их результаты и эффективность. Если бот работает в режиме симуляции (`is_trading_mode = False`), то он должен корректно регистрировать историю симулированных торговых операций.
4. **Сбор данных (Data Collection Module):** Бот должен успешно собирать необходимые торговые данные с MOEX в реальном времени в соответствии с заданным периодом.
5. **Очистка данных (Data Cleaning Module):** Бот должен успешно очищать и форматировать полученные торговые данные перед передачей в следующий модуль.
6. **Извлечение признаков (Feature Extraction Module):** Бот должен успешно извлекать признаки из очищенных торговых данных.
7. **Оценка рисков (Risk Assessment Module):** Бот должен успешно оценивать риски на основе извлеченных признаков.
8. **Прогнозирование рынка (Market Prediction Module):** Бот должен успешно прогнозировать движение рынка на основе извлеченных признаков и оценки рисков.
9. **Определение торговой стратегии (Trading Strategy Module):** Бот должен успешно определять торговую стратегию на основе прогнозов рынка и оценки рисков.
10. **Исполнение торговых операций (Order Execution Module):** Бот должен успешно отправлять сигналы для исполнения торговых операций. В режиме реальной торговли, операции должны быть корректно выполнены через брокера на MOEX, а в режиме симуляции — корректно симулированы.
11. **Измерение эффективности (Effectiveness Measurement Module):** Бот должен корректно измерять эффективность текущей стратегии, опираясь на историю торговых операций.
12. **Адаптация стратегии и модели (Strategy and Model Adaptation Module):** В режиме симуляции, бот должен корректно адаптировать стратегию и модель на основе измеренной эффективности.
13. **Управление ботом через Telegram:** Все команды, отправляемые пользователем через Telegram, должны быть успешно интерпретированы и выполнены ботом. Бот должен немедленно отправлять обратную связь о статусе и результате выполнения команд.
14. **Автоматическая остановка:** Бот должен автоматически останавливаться при достижении заданного порога эффективности в режиме реальной торговли (`is_trading_mode = True`), как указано в настройках.
